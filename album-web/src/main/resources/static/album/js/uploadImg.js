$(document).ready(function(){
    var albumCount = 0;
    var albumTotal = 0;
    $("#chooseAlbum").on("click",function () {
        if($("#uploadImageBtn").hasClass("layui-btn-disabled") == false) {
            $("#uploadImageBtn").addClass("layui-btn-disabled");
        }
    })
    layui.use('upload', function () {
        var $ = layui.jquery , upload = layui.upload;
        var files;
        //多图片上传
        upload.render({
            elem: '#chooseAlbum'
            , url: '/filetransfer/uploadimage'
            , data: {
                albumid: function () {
                    thisURL = document.URL;
                    var getval = thisURL.split('?')[1].split("&")[0];
                    albumId = getval.split("=")[1];
                    if (albumId.indexOf("#") != -1) {
                        albumId = albumId.substring(0, albumId.indexOf("#"));
                    }
                    return albumId;
                }
            }
            , multiple: true
            , auto: false
            , bindAction: '#uploadImageBtn'
            , choose: function (obj) {
                files = obj.pushFile(); //将每次选择的文件追加到文件队列
                //预读本地文件示例，不支持ie8
                obj.preview(function (index, file, result) {
                    $('#demo2').append('<img src="' + result + '" alt="' + file.name + '" class="layui-upload-img">')
                    albumTotal++;
                });
                $("#uploadImageBtn").removeClass("layui-btn-disabled"); //取消上传按钮的禁用状态
            }
            , before: function (obj) {

            }
            , done: function (res,index) {
                albumCount++;
                $(".photoIndex").text(albumCount);  //第几张照片
                $(".photoName").text(files[index].name); //从文件队列中根据索引取出名称
            }
            ,allDone: function(obj){ //当文件全部被提交后，才触发
                alertMsg("上传文件数量：" + obj.total + "  成功：" + obj.successful + "   失败：" + obj.aborted);
                $(".layui-layer-btn0").on("click",function () { // 监听确定按钮
                    var _url = thisURL.split('?')[1];
                    var photoCount = Number(localStorage.getItem("imageCount"))+albumTotal;
                    console.log(photoCount);
                    window.location.href = "showAlbum.html?"+_url+"&imageCount="+photoCount;
                });
            }
        });
    });
    //上传进度条
    layui.use('element', function(){
        var $ = layui.jquery ,element = layui.element; //Tab的切换功能，切换事件监听等，需要依赖element模块
        //触发事件
        var active = {
            loading: function(othis){ //模拟loading
                var timer = setInterval(function(){
                    if(albumCount>albumTotal){
                        albumCount = albumTotal;
                        clearInterval(timer);
                    }
                    element.progress('demo', parseInt(albumCount/albumTotal*100)+'%');
                }, 10);
            }
        };

        $('#uploadImageBtn').on('click', function(){
            var othis = $(this), type = $(this).data('type');
            active[type] ? active[type].call(this, othis) : '';
            $(".progressShowWrapper").css("display","inline-block"); //显示进度条
        });
    });
});
