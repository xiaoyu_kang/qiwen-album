$(document).ready(function () {
    $(".btnLogin").hover(
        function () {
            $(this).css("backgroundColor", "rgba(20,27,97,0.63)");
        },
        function () {
            $(this).css("backgroundColor", "#141b61");
        }
    );

});

//用户登陆
function loginUserInfo() {

    var loginUserName = document.getElementById("username").value;
    var loginPassword = document.getElementById("password").value;


    var userInfo = {
        username: loginUserName,
        password: loginPassword
    };

    $.ajax({
        data: userInfo,
        url: "/user/userlogin",
        dataType: "json",
        type: "POST",
        async: true,
        success: function (result) {
            if (result.success) {
                console.log("成功");
                //alert("登录成功");
                window.location.href = "/index.html";
            } else {
                console.log("失败");
                //alert(result.errorMessage);
            }
        },
        error: function () {
            console.log("繁忙");
            ////alert("系统繁忙!");
        }
    });
}

