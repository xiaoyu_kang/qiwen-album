$(document).ready(function () {
    addressInit('cmbProvince', 'cmbCity', 'cmbArea');
});


//初始化用户信息
function initUserInfo(userId) {

    var userJson = {};
    userJson["userId"] = userId;

    $.ajax({
        data: userJson,
        url: "/user/getUserInfoById",
        dataType: "json",
        type: "POST",
        success: function (result) {
            if (result.success) {
                var province = result.data.addrprovince == undefined ? "" : result.data.addrprovince;
                var city = result.data.addrcity == undefined ? "" : result.data.addrcity;
                var area = result.data.addrarea == undefined ? "" : result.data.addrarea;
                var birthday = result.data.birthday == undefined ? "" : result.data.birthday;
                var sex = result.data.sex == undefined ? "" : result.data.sex;
                var position = result.data.position == undefined ? "" : result.data.position;
                var intro = result.data.intro == undefined ? "" : result.data.intro;
                var username = result.data.username == undefined ? "" : result.data.username;
                var realname = result.data.realname == undefined ? "" : result.data.realname;
                var userImage = result.data.imageBeanList == undefined ? "" : "/filetransfer/" + result.data.imageBeanList[result.data.imageBeanList.length - 1].imageurl;

                document.getElementById("usernameInput").setAttribute("value", username);
                document.getElementById("realnameInput").setAttribute("value", realname);
                document.getElementById("positionInput").setAttribute("value", position);
                document.getElementById("birthdayInput").setAttribute("value", birthday);
                parent.document.getElementById("userImageId").setAttribute("src", userImage);
                document.getElementById("introTextarea").value = intro;
                if (result.data.sex == "男") {
                    document.getElementById("sexMan").setAttribute("checked", "checked");
                } else {
                    document.getElementById("sexwoman").setAttribute("checked", "checked");
                }

                addressInit('cmbProvince', 'cmbCity', 'cmbArea', result.data.addrprovince, result.data.addrcity, result.data.addrarea);
            } else {
                alert("失敗" + result);
            }
        },
        error: function (result) {
            //alert("系统繁忙!");
        }
    });
}

/**
 * 保存用户信息
 * @returns
 */
function saveUserInfoClick() {
    var userName = document.getElementById("usernameInput").value;
    var realName = document.getElementById("realnameInput").value;
    var position = document.getElementById("positionInput").value;
    var sex = document.getElementById("sexMan").getAttribute("checked") == "checked" ? "男" : "女";
    var birthday = document.getElementById("birthdayInput").value;
    var introTextarea = document.getElementById("introTextarea").value;
    var cmbProvince = document.getElementById("cmbProvince").value;
    var cmbCity = document.getElementById("cmbCity").value;
    var cmbArea = document.getElementById("cmbArea").value;
    var userInfo = {};
    userInfo["username"] = userName;
    userInfo["realname"] = realName;
    userInfo["position"] = position;
    userInfo["sex"] = sex;
    userInfo["birthday"] = birthday;
    userInfo["intro"] = introTextarea;
    userInfo["addrprovince"] = cmbProvince;
    userInfo["addrcity"] = cmbCity;
    userInfo["addrarea"] = cmbArea;

    var checkPass = checkSaveUserInfo(userInfo);
    if (!checkPass) {
        return;
    }

    $.ajax({
        data: userInfo,
        url: "/user/updateUserInfo",
        dataType: "json",
        type: "POST",
        success: function (result) {
            if (result.success) {
                alert("保存成功！");

            } else {
                alert("失敗" + result);
            }
        },
        error: function (result) {
            //alert("系统繁忙!");
        }
    });

}

/**
 * 保存用户信息参数校验
 * @param userInfo
 * @returns
 */
function checkSaveUserInfo(userInfo) {
    var userName = userInfo["username"];
    if (userName.trim() == "") {
        alert("用户名不能为空");
        return false;
    }
    return true;
}

/**
 * 性别选择事件
 * @param e
 * @returns
 */
function clickSexRadio(e) {
    if (e.id == "sexMan") {
        document.getElementById("sexMan").setAttribute("checked", "checked");
        document.getElementById("sexwoman").setAttribute("checked", "");
    } else {
        document.getElementById("sexwoman").setAttribute("checked", "checked");
        document.getElementById("sexMan").setAttribute("checked", "");
    }
}


//纯JS省市区三级联动
//2011-11-30 by http://www.cnblogs.com/zjfree
var addressInit = function (_cmbProvince, _cmbCity, _cmbArea, defaultProvince,
                            defaultCity, defaultArea) {
    var cmbProvince = document.getElementById(_cmbProvince);
    var cmbCity = document.getElementById(_cmbCity);
    var cmbArea = document.getElementById(_cmbArea);

    function cmbSelect(cmb, str) {
        for (var i = 0; i < cmb.options.length; i++) {
            if (cmb.options[i].value == str) {
                cmb.selectedIndex = i;
                return;
            }
        }
    }

    function cmbAddOption(cmb, str, obj) {
        var option = document.createElement("OPTION");
        cmb.options.add(option);
        option.innerText = str;
        option.value = str;
        option.obj = obj;
    }

    function changeCity() {
        cmbArea.options.length = 0;
        if (cmbCity.selectedIndex == -1)
            return;
        var item = cmbCity.options[cmbCity.selectedIndex].obj;
        for (var i = 0; i < item.areaList.length; i++) {
            cmbAddOption(cmbArea, item.areaList[i], null);
        }
        cmbSelect(cmbArea, defaultArea);
    }

    function changeProvince() {
        cmbCity.options.length = 0;
        cmbCity.onchange = null;
        if (cmbProvince.selectedIndex == -1)
            return;
        var item = cmbProvince.options[cmbProvince.selectedIndex].obj;
        for (var i = 0; i < item.cityList.length; i++) {
            cmbAddOption(cmbCity, item.cityList[i].name, item.cityList[i]);
        }
        cmbSelect(cmbCity, defaultCity);
        changeCity();
        cmbCity.onchange = changeCity;
    }

    for (var i = 0; i < provinceList.length; i++) {
        cmbAddOption(cmbProvince, provinceList[i].name, provinceList[i]);
    }
    cmbSelect(cmbProvince, defaultProvince);
    changeProvince();
    cmbProvince.onchange = changeProvince;
};  