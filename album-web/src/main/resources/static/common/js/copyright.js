var footDiv = "<div class='footer'>\n" +
    "    <div class='layui-container'>\n" +
    "        <p class='footer-web'>\n" +
    "            <a href='javascript:;'>合作伙伴</a>\n" +
    "            <a href='javascript:;'>企业画报</a>\n" +
    "            <a href='javascript:;'>JS网</a>\n" +
    "            <a href='javascript:;'>千图网</a>\n" +
    "            <a href='javascript:;'>昵图网</a>\n" +
    "            <a href='javascript:;'>素材网</a>\n" +
    "            <a href='javascript:;'>花瓣网</a>\n" +
    "        </p>\n" +
    "        <div class='layui-row footer-contact'>\n" +
    "            <div class='layui-col-sm2 layui-col-lg1'><img style='width: 86px; height: 86px;' src='/common/image/erweima.png'></div>\n" +
    "            <div class='layui-col-sm10 layui-col-lg11'>\n" +
    "                <div class='layui-row'>\n" +
    "                    <div class='layui-col-sm6 layui-col-md8 layui-col-lg9'>\n" +
    "                        <p class='contact-top'><i class='layui-icon layui-icon-cellphone'></i>&nbsp;18829291817&nbsp;&nbsp;(9:00-18:00)</p>\n" +
    "                        <p class='contact-bottom'><i class='layui-icon layui-icon-home'></i>&nbsp;1162714483@qq.com</span></p>\n" +
    "                    </div>\n" +
    "                    <div class='layui-col-sm6 layui-col-md4 layui-col-lg3'>\n" +
    "                        <p class='contact-top'><span class='right'>该模板版权归 <a href='https://www.layui.com/' target='_blank'>layui.com</a> 所有</span></p>\n" +
    "                        <p class='contact-bottom'><span class='right'>Copyright&nbsp;©&nbsp;2016-2018&nbsp;&nbsp;ICP&nbsp;备888888号</span></p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>";

$(document).ready(function () {
    var divNode2 = document.createElement("div");
    divNode2.setAttribute("id", "navbottomBarId");
    document.body.appendChild(divNode2);
    document.getElementById("navbottomBarId").innerHTML = footDiv;
});