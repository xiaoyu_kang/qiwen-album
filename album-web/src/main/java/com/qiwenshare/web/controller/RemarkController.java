package com.qiwenshare.web.controller;

import com.alibaba.fastjson.JSON;
import com.qiwenshare.common.cbb.RestResult;
import com.qiwenshare.web.anno.MyLog;
import com.qiwenshare.web.api.IRemarkService;
import com.qiwenshare.web.domain.RemarkBean;
import com.qiwenshare.web.domain.ReplyBean;
import com.qiwenshare.web.domain.UserBean;
import org.apache.shiro.SecurityUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/remark")
public class RemarkController {

    /**
     * 当前模块
     */
    public static final String CURRENT_MODULE = "评论管理";
    @Resource
    IRemarkService remarkService;


    /**
     * 添加评论
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/addremark", method = RequestMethod.POST)
    @ResponseBody
    @MyLog(operation = "添加评论", module = CURRENT_MODULE)
    public RestResult<String> addRemark(@RequestBody RemarkBean remarkBean) {
        RestResult<String> restResult = new RestResult<String>();
        UserBean sessionUserBean = (UserBean) SecurityUtils.getSubject().getPrincipal();

        if (sessionUserBean == null) {
            restResult.setSuccess(false);
            restResult.setErrorMessage("用户没有登录");
            return restResult;
        }


        Date date = new Date();
        String stringDate = String.format("%tF %<tT", date);

        remarkBean.setRemarkUserId(sessionUserBean.getUserId());
        remarkBean.setRemarkDate(stringDate);
        boolean result = remarkService.addRemark(remarkBean);
        if (result) {
            restResult.setData("评论成功");
            restResult.setSuccess(true);
        } else {
            restResult.setErrorMessage("评论失败");
            restResult.setSuccess(true);
        }

        return restResult;
    }

    /**
     * 添加回复
     *
     * @param request
     * @param replyBean
     * @return
     */
    @RequestMapping(value = "/addreply", method = RequestMethod.POST)
    @ResponseBody
    @MyLog(operation = "添加回复", module = CURRENT_MODULE)
    public RestResult<String> addReply(@RequestBody ReplyBean replyBean) {
        RestResult<String> restResult = new RestResult<String>();
        Date date = new Date();
        String stringDate = String.format("%tF %<tT", date);
        UserBean sessionUserBean = (UserBean) SecurityUtils.getSubject().getPrincipal();
        if (sessionUserBean == null) {
            restResult.setSuccess(false);
            restResult.setErrorMessage("用户没有登录");
            return restResult;
        }

        long replyUserId = sessionUserBean.getUserId();

        replyBean.setReplyDate(stringDate);
        replyBean.setReplyUserId(replyUserId);

        boolean result = remarkService.addReply(replyBean);
        if (result) {
            restResult.setData("回复成功");
            restResult.setSuccess(true);
        } else {
            restResult.setErrorMessage("回复失败");
            restResult.setSuccess(true);
        }

        return restResult;
    }

    /**
     * 获取评论列表
     *
     * @return
     */
    @RequestMapping(value = "/getremarkcontent", method = RequestMethod.GET)
    @ResponseBody
    public String getRemarkContent(RemarkBean remarkBean) {
        RestResult<List<RemarkBean>> restResult = new RestResult<List<RemarkBean>>();
        List<RemarkBean> remarkConent = remarkService.getRemarkByTargetId(remarkBean);

        restResult.setData(remarkConent);
        restResult.setSuccess(true);
        String resultJson = JSON.toJSONString(restResult);
        return resultJson;
    }

    /**
     * 删除评论
     *
     * @return
     */
    @RequestMapping(value = "/deleteremark", method = RequestMethod.POST)
    @ResponseBody
    @MyLog(operation = "删除评论", module = CURRENT_MODULE)
    public String deleteRemark(@RequestBody int remarkId) {
        RestResult<String> restResult = new RestResult<String>();
        remarkService.delReplyByRemarkId(remarkId);
        remarkService.delRemarkByRemarkId(remarkId);

        restResult.setSuccess(true);
        String resultJson = JSON.toJSONString(restResult);
        return resultJson;
    }

}
