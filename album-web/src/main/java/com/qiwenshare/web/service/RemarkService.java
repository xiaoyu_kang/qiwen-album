package com.qiwenshare.web.service;

import com.qiwenshare.web.api.IRemarkService;
import com.qiwenshare.web.domain.RemarkBean;
import com.qiwenshare.web.domain.ReplyBean;
import com.qiwenshare.web.domain.UserBean;
import com.qiwenshare.web.mapper.RemarkMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class RemarkService implements IRemarkService {
    @Resource
    RemarkMapper remarkMapper;

    @Override
    public boolean addRemark(RemarkBean remarkBean) {
        boolean result = remarkMapper.addRemark(remarkBean);
        return result;
    }

    @Override
    public boolean addReply(ReplyBean replyBean) {
        boolean result = remarkMapper.addReply(replyBean);
        return result;
    }

    @Override
    public List<RemarkBean> getRemarkByTargetId(RemarkBean remarkBean) {
        List<RemarkBean> list = remarkMapper.getRemarkByTargetId(remarkBean);
        return list;
    }

    @Override
    public boolean delRemarkByRemarkId(long remarkId) {
        boolean result = remarkMapper.delRemarkByRemarkId(remarkId);
        return result;
    }

    @Override
    public void delRemarkByTargetId(long targetid) {
        RemarkBean remarkBean = new RemarkBean(targetid, "essay");
        List<RemarkBean> remarkList = getRemarkByTargetId(remarkBean);
        for (int i = 0; i < remarkList.size(); i++) {
            delReplyByRemarkId(remarkList.get(i).getRemarkId());

        }
        remarkMapper.delRemarkByTargetId(targetid);
    }

    @Override
    public boolean delReplyByRemarkId(long remarkId) {
        boolean result = remarkMapper.delReplyByRemarkId(remarkId);
        return result;
    }

    @Override
    public int selectRemarkCountByUser(UserBean userBean) {
        int result = remarkMapper.selectRemarkCountByUser(userBean);
        return result;
    }
}
