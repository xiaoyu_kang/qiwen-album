package com.qiwenshare.web.config.websocket;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker //支持WebSocket消息处理，由消息代理支持
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    /**
     * 配置消息代理
     * @param config
     */
    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        //表示客户端订阅地址的前缀信息，也就是客户端接收服务端消息的地址的前缀信息(比较绕)
        config.enableSimpleBroker("/topic", "/user");
        //指服务端接收地址的前缀，意思就是说客户端给服务端发消息的地址的前缀
        config.setApplicationDestinationPrefixes("/app");
        //指推送用户前缀。
        config.setUserDestinationPrefix("/user/");
    }

    /**
     * 启用SockJS后备选项，以便在WebSocket不可用时可以使用备用传输
     * @param registry
     */
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/webServer").setAllowedOrigins("*").withSockJS();
    }

}