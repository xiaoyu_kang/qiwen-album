package com.qiwenshare.web.domain;

import javax.persistence.*;
import java.util.List;

/**
 * 评论实体类
 *
 * @author ma116
 */
@Table(name = "remark")
@Entity
public class RemarkBean {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long remarkId;
    @Column
    private long essayId;

    private long targetid;

    private String modulename;

    @Column
    private String remarkContent;
    @Column
    private String remarkDate;
    @Column
    private long remarkUserId;
    @Column
    private String remarkUserContent;

    @Transient
    private UserBean userBean;
    @Transient
    private List<ReplyBean> replyList;

    public RemarkBean() {
    }

    public RemarkBean(long targetid, String modulename) {
        this.targetid = targetid;
        this.modulename = modulename;
    }

    public String getModulename() {
        return modulename;
    }

    public void setModulename(String modulename) {
        this.modulename = modulename;
    }

    public long getTargetid() {
        return targetid;
    }

    public void setTargetid(long targetid) {
        this.targetid = targetid;
    }

    public long getRemarkId() {
        return remarkId;
    }

    public void setRemarkId(long remarkId) {
        this.remarkId = remarkId;
    }

    public long getEssayId() {
        return essayId;
    }

    public void setEssayId(long essayId) {
        this.essayId = essayId;
    }

    public String getRemarkContent() {
        return remarkContent;
    }

    public void setRemarkContent(String remarkContent) {
        this.remarkContent = remarkContent;
    }

    public String getRemarkDate() {
        return remarkDate;
    }

    public void setRemarkDate(String remarkDate) {
        this.remarkDate = remarkDate;
    }

    public long getRemarkUserId() {
        return remarkUserId;
    }

    public void setRemarkUserId(long remarkUserId) {
        this.remarkUserId = remarkUserId;
    }

    public String getRemarkUserContent() {
        return remarkUserContent;
    }

    public void setRemarkUserContent(String remarkUserContent) {
        this.remarkUserContent = remarkUserContent;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public List<ReplyBean> getReplyList() {
        return replyList;
    }

    public void setReplyList(List<ReplyBean> replyList) {
        this.replyList = replyList;
    }
}
