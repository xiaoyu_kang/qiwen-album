package com.qiwenshare.web.api;

import com.qiwenshare.web.domain.RemarkBean;
import com.qiwenshare.web.domain.ReplyBean;
import com.qiwenshare.web.domain.UserBean;

import java.util.List;

public interface IRemarkService {
    /**
     * 添加评论
     *
     * @param remarkBean 评论信息
     * @return 是否评论成功
     */
    public boolean addRemark(RemarkBean remarkBean);

    /**
     * 获取评论通过目标id
     * @param remarkBean 评论信息
     * @return 评论列表
     */
    public List<RemarkBean> getRemarkByTargetId(RemarkBean remarkBean);

    /**
     * 添加回复
     *
     * @param replyBean 回复信息
     * @return 是否成功
     */
    public boolean addReply(ReplyBean replyBean);

    /**
     * 删除回复
     *
     * @param remarkId 评论id
     * @return 是否删除成功
     */
    boolean delRemarkByRemarkId(long remarkId);

    /**
     * 删除评论通过目标id
     * @param targetid 目标id
     */
    void delRemarkByTargetId(long targetid);

    /**
     * 删除回复通过评论id
     *
     * @param remarkId 评论id
     * @return 是否删除成功
     */
    boolean delReplyByRemarkId(long remarkId);

    /**
     * 查用户的评论数量
     *
     * @param userBean 用户信息
     * @return 评论数量
     */
    int selectRemarkCountByUser(UserBean userBean);
}
